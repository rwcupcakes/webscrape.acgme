# getPrograms.py

'''------------------------------
IMPORTS
------------------------------'''
# Beautiful Soup
# https://www.crummy.com/software/BeautifulSoup/bs4/doc/
import bs4
# URL handling
# https://docs.python.org/3/library/urllib.html
import urllib.request
# JSON 
import json
# CSV Reads
# https://pandas.pydata.org/pandas-docs/stable/generated/pandas.read_csv.html
import pandas
import os.path


# data json
institutions_json = {}

# Root URL for ACGME website to scrape data from
root_url = 'https://services.aamc.org/eras/erasstats/par/'

# User Agent Header request
user_agent = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)'
headers = { 'User-Agent' : user_agent }

# Make request to get HTML from server
request = urllib.request.Request(root_url, None, headers )
source = urllib.request.urlopen( request ).read()

# source = urllib.request.urlopen( root_url, headers ).read()
# Turn raw HTML text to parsable markup
soup = bs4.BeautifulSoup(source, 'lxml')

# Find all Tables that contain the class .pplcell
tableCycles = soup.find_all('table', {'class': 'pplcell'})

# print(len(tableCycles))
# print(tableCycles)

for table in tableCycles:

    if table.find('table', {'class': 'pplcell'}):
        degreeType_cycle = table.find('table', {'class': 'pplcell'})

        font = degreeType_cycle.b.find('font')
        font = font.decode_contents(formatter='html')
        font = font.replace('<b>', '')
        font = font.replace('</b>', '')

        title = font.split(' - ')
        cycle = title[1].strip()
        cycle = cycle.replace('Cycle', '')

        programs_json.update({
            font : {
                'title': title[0].strip(),
                'cycle': cycle.strip(),
                'total': '',
                'specialty': {},
                'specialty_names': []
            }
        })


        specialties = table.find_all('a')
        total_specialties = len(specialties)
        print(total_specialties)

        for specialty in specialties:

            link = specialty.get('href')

            programs_json[font]['specialty_names'].append(specialty.decode_contents(formatter='html'))

            programs_json[font]['specialty'].update({
                specialty.decode_contents(formatter='html') : {
                    'root': root_url + link
                }
            })


# Print JSON Result
# print(json.dumps(programs_json, indent=4, sort_keys=True))

    
#     links.update({title[0].strip() : {
#         'name': title[1].strip(),
#         'root': a.get('href'),
#         'classes': {}
#     }})

# for k in links:
#     print(links[k]['name'])
#     url = links[k]['root']
#     nSource = urllib.request.urlopen( url ).read()
#     soup = bs4.BeautifulSoup(nSource, 'lxml')
    
#     for a in soup.section.section.find_all('a'):
#         title = a.div.div.text.split(' - ')
        
#         links[k]['classes'].update({title[0].strip() : {
#             'name': title[1].strip(),
#             'root': a.get('href'),
#             'sections': {}
#         }})
        
# with open('courses.json', 'w') as outfile:
#     json.dump(links, outfile)
    
# print(json.dumps(links))