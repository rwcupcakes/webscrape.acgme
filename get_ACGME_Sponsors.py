# get_ACGME_Sponsors.py


international = "https://apps.acgme-i.org/ads/public/intl"

'''
------------------------------
IMPORTS
------------------------------
'''
# Beautiful Soup
# https://www.crummy.com/software/BeautifulSoup/bs4/doc/
import bs4
# URL handling
# https://docs.python.org/3/library/urllib.html
import urllib.request
# JSON 
import json
# CSV Reads
# https://pandas.pydata.org/pandas-docs/stable/generated/pandas.read_csv.html
import pandas
import os.path
from io import StringIO


import ssl


'''
---------------------------
MAIN
---------------------------
'''
institutions_csv = './ACGME_Institutions.csv'
# institutions_csv = './ACGME_Institutions.test.csv'

# CSV String
CSV = ''
# Program Counter
total = 1

# User Agent Header request
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)'
}

# SSL Certs
context = ssl._create_unverified_context()




'''
---------------------------
OBJECT
---------------------------
'''
institution = {
    # Institution Link
    'Link': '',
    # Institution ACGME ID Key
    'sponsorId_key': '',
    # Institution Sponsor ID
    'Sponsoring Institution ID': '',
    # Institution State
    'State': '',
    # Institution Code
    'Code': '',
    # Institution Name
    'Name': '',

    # Institution Number of Accredited Programs
    'Accredited Programs': '',

    # Institution
    'Accreditation Status': '',
    # Institution Effective Date
    'Effective Date': '',

    # Institution Last Site Visit Date
    'Last Site Visit Date': '',
    # Institution Date of Next Site Visit
    'Date of Next Site Visit': '',
    # Institution Self Study Due Date
    'Self Study Due Date': '',
    # Institution 10 Year Site Visit
    '10 Year Site Visit': '',
    # Institution Last CLER Site Visit Date
    'Last CLER Site Visit Date': '',
    # Institution Next CLER Site Visit Date
    'Next CLER Site Visit Date': '',

    # -------------------------------

    # Institution HIPAA Business Associate Agreement On Record
    'HIPAA Business Associate Agreement On Record': '',
    # Institution Residents Rotate Through This Institution
    'Residents Rotate Through This Institution': '',

    # Institution CEO
    'CEO - Name': '',
    'CEO - Title': '',
    'CEO - Phone': '',
    'CEO - Fax': '',
    'CEO - Email': '',

    # Institution DIO
    'DIO - Full Name': '',
    'DIO - Last Name': '',
    'DIO - Title': '',
    'DIO - Phone': '',
    'DIO - Fax': '',
    'DIO - Email': '',

    # Institution Address
    'Address': '',
    # Institution Web Site
    'Web Site': '',
    # Institution Recognized By
    'Recognized By': '',
    # Institution Ownership or Control Type
    'Ownership or Control Type': '',
    # Institution Type of Institution
    'Type of Institution': '',
    # Institution Affiliated Medical Schools
    'Affiliated Medical Schools': '',
    
    
    # Sponsored Program
    'Sponsored Program - Code': '',
    'Sponsored Program - Specialty': '',
    'Sponsored Program - PD Last Name': '',
    'Sponsored Program - Total Residents Filled': '',
    'Sponsored Program - Program Link': '',
}

'''''''''
CSV HEADERS
'''''''''
CSV += '"Total","iTotal",'
for key, value in institution.items():
    # KEY & VALUE
    CSV += '"' + key+ '",'

# Remove final ',' from CSV String
CSV = CSV[:-1]


# READ CSV (ACGME_Institutions)
institutions = pandas.read_csv(
    institutions_csv, 
	quotechar = '"', 
	skipinitialspace = False, 
	delimiter = ',', 
	encoding = 'utf-8',
	index_col = False,
	low_memory = False,
	names=['total',
		'state', 
		'institution_code', 
		'institution_name', 
		'institution_dioLastname', 
		'institution_accreditedPrograms', 
		'institution_status', 
		'institution_link'
        ]
    )

#  For each Institution
for index, row in institutions.iterrows():
    print('iTotal: ' + str(row['total']))
    
    if row['institution_link']:
        # Link URL
        URL = row['institution_link']


        # Institution
        institution = {
            # Institution Link
            'Link': '',
            # Institution ACGME ID Key
            'sponsorId_key': '',
            # Institution Sponsor ID
            'Sponsoring Institution ID': '',
            # Institution State
            'State': '',
            # Institution Code
            'Code': '',
            # Institution Name
            'Name': '',

            # Institution Number of Accredited Programs
            'Accredited Programs': '',

            # Institution
            'Accreditation Status': '',
            # Institution Effective Date
            'Effective Date': '',

            # Institution Last Site Visit Date
            'Last Site Visit Date': '',
            # Institution Date of Next Site Visit
            'Date of Next Site Visit': '',
            # Institution Self Study Due Date
            'Self Study Due Date': '',
            # Institution 10 Year Site Visit
            '10 Year Site Visit': '',
            # Institution Last CLER Site Visit Date
            'Last CLER Site Visit Date': '',
            # Institution Next CLER Site Visit Date
            'Next CLER Site Visit Date': '',

            # -------------------------------

            # Institution HIPAA Business Associate Agreement On Record
            'HIPAA Business Associate Agreement On Record': '',
            # Institution Residents Rotate Through This Institution
            'Residents Rotate Through This Institution': '',

            # Institution CEO
            'CEO - Name': '',
            'CEO - Title': '',
            'CEO - Phone': '',
            'CEO - Fax': '',
            'CEO - Email': '',

            # Institution DIO
            'DIO - Full Name': '',
            'DIO - Last Name': '',
            'DIO - Title': '',
            'DIO - Phone': '',
            'DIO - Fax': '',
            'DIO - Email': '',

            # Institution Address
            'Address': '',
            # Institution Web Site
            'Web Site': '',
            # Institution Recognized By
            'Recognized By': '',
            # Institution Ownership or Control Type
            'Ownership or Control Type': '',
            # Institution Type of Institution
            'Type of Institution': '',
            # Institution Affiliated Medical Schools
            'Affiliated Medical Schools': '',
            
            
            # Sponsored Program
            'Sponsored Program - Code': '',
            'Sponsored Program - Specialty': '',
            'Sponsored Program - PD Last Name': '',
            'Sponsored Program - Total Residents Filled': '',
            'Sponsored Program - Program Link': '',
        }


        # DATE
        '''''''''''''''''''''
        Information from the Original CSV
        '''''''''''''''''''''

        institution['Link']                      = str(row['institution_link'])

        institution['sponsorId_key']             = str(URL.split('=')[1])

        institution['Sponsoring Institution ID'] = str(row['institution_code'])

        institution['State']                     = str(row['state'])

        institution['Name']                      = str(row['institution_name'])

        institution['Accredited Programs']       = str(row['institution_accreditedPrograms'])

        institution['Accreditation Status']      = str(row['institution_status'])

        institution['DIO - Last Name']          = str(row['institution_dioLastname'])



        # REQUEST
        '''''''''''''''''''''
        REQUEST
        '''''''''''''''''''''

        # Make request to get HTML from server
        request = urllib.request.Request(URL, None, headers)

        # Request HTML
        source = urllib.request.urlopen(request, context=context).read()

        # Turn raw HTML text to parsable markup
        soup = bs4.BeautifulSoup(source, 'lxml')

        CONTENT = soup.find('div', {'id': 'content-panel'})

        ''' Institution Code '''
        header = CONTENT.find('div', {'class': 'page-header'})
        header = str(header.h1.small.text.strip())

        header = header.split('-')
        header_Code = header[0].strip()
    
        # Institution Code
        institution['Code'] = header_Code

        '''''''''''''''''''''
        DIVS
        '''''''''''''''''''''
        DIVS = CONTENT.findAll('div', {'class': None})

        
        ''' Institution Effective Date '''
        section = DIVS[0]
        section = section.findAll('dd')
        # Institution Effective Date
        institution['Effective Date'] = str(section[1].text.strip())


        ''' Last Site Visit Date '''
        ''' Date of Next Site Visit '''
        ''' Self Study Due Date '''
        ''' 10 Year Site Visit '''
        ''' Last CLER Site Visit Date '''
        ''' Next CLER Site Visit Date '''
        section = DIVS[1]
        section = section.findAll('dd')
        if len(section) == 4:
            # Institution Last Site Visit Date
            institution['Last Site Visit Date'] = str(section[0].text.strip())
            # Institution Date of Next Site Visit
            institution['Date of Next Site Visit'] = str(section[1].text.strip())
            # Institution Last CLER Site Visit Date
            institution['Last CLER Site Visit Date'] = str(section[2].text.strip())
            # Institution Next CLER Site Visit Date
            institution['Next CLER Site Visit Date'] = str(section[3].text.strip())
        elif len(section) == 6:
            # Institution Last Site Visit Date
            institution['Last Site Visit Date'] = str(section[0].text.strip())
            # Institution Date of Next Site Visit
            institution['Date of Next Site Visit'] = str(section[1].text.strip())

            # Institution Self Study Due Date
            institution['Self Study Due Date'] = str(section[2].text.strip())
            # Institution 10 Year Site Visit
            institution['10 Year Site Visit'] = str(section[3].text.strip())

            # Institution Last CLER Site Visit Date
            institution['Last CLER Site Visit Date'] = str(section[4].text.strip())
            # Institution Next CLER Site Visit Date
            institution['Next CLER Site Visit Date'] = str(section[5].text.strip())
        else:
            print('ERROR 01: ' + str(total))
        


        ''' HIPAA Business Associate Agreement On Record '''
        ''' Residents Rotate Through This Institution '''
        section = DIVS[2]
        section = section.findAll('dd')
        if len(section) == 2:
            institution['HIPAA Business Associate Agreement On Record'] = str(section[0].text.strip())
            institution['Residents Rotate Through This Institution'] = str(section[1].text.strip())
        else:
            print('ERROR 02: ' + str(total))


        ''' CEO '''
        section = DIVS[3]
        section = section.findAll('dd')
        if len(section) == 4:
            # Institution CEO - Name
            institution['CEO - Name'] = str(section[0].text.strip())
            # Institution CEO - Title
            institution['CEO - Title'] = str(section[1].text.strip())
            # Institution CEO - Phone
            institution['CEO - Phone'] = str(section[2].text.strip())
            # Institution CEO - Email
            institution['CEO - Email'] = str(section[3].text.strip())
        elif len(section) == 5:
            # Institution CEO - Name
            institution['CEO - Name'] = str(section[0].text.strip())
            # Institution CEO - Title
            institution['CEO - Title'] = str(section[1].text.strip())
            # Institution CEO - Phone
            institution['CEO - Phone'] = str(section[2].text.strip())
            # Institution CEO - Fax
            institution['CEO - Fax'] = str(section[3].text.strip())
            # Institution CEO - Email
            institution['CEO - Email'] = str(section[4].text.strip())
        else:
            print('ERROR 03: ' + str(total))


        ''' DIO '''
        section = DIVS[4]
        section = section.findAll('dd')
        if len(section) == 5:
            # Institution DIO - Full Name
            institution['DIO - Full Name'] = str(section[0].text.strip())
            # Institution DIO - Title
            institution['DIO - Title'] = str(section[1].text.strip())
            # Institution DIO - Phone
            institution['DIO - Phone'] = str(section[2].text.strip())
            # Institution DIO - Fax
            institution['DIO - Fax'] = str(section[3].text.strip())
            # Institution DIO - Email
            institution['DIO - Email'] = str(section[4].text.strip())
        elif len(section) == 4:
            # Institution DIO - Full Name
            institution['DIO - Full Name'] = str(section[0].text.strip())
            # Institution DIO - Title
            institution['DIO - Title'] = str(section[1].text.strip())
            # Institution DIO - Phone
            institution['DIO - Phone'] = str(section[2].text.strip())
            # Institution DIO - Email
            institution['DIO - Email'] = str(section[3].text.strip())
        else:
            print('ERROR 04: ' + str(total))


        ''' Address '''
        SPONSORSUMMARY = CONTENT.find('div', {'class': 'well'})
        # print('ERROR 05: ' + str(total))
        address = SPONSORSUMMARY.address.text.strip()
        address = address.replace('  ', '')
        address = address.replace('\r', '')
        address = address.split('\n')
        for i, line in enumerate(address):
            if i+1 == len(address):
                # Institution Address
                institution['Address'] += str(line.strip())
            else:
                # Institution Address
                institution['Address'] += str(line.strip()) + ' '

        ''' Web Site '''
        if SPONSORSUMMARY.find('a'):
            site = SPONSORSUMMARY.a['href'].strip()
            # Institution Web Site
            institution['Web Site'] = str(site)
        else:
            print('ERROR 06: ' + str(total))
        

        '''''''''
        LAST PART
        '''''''''
        last = SPONSORSUMMARY.findAll('dl', {'class': 'dl-horizontal'})


        ''' Recognized By '''
        ''' Ownership or Control Type '''
        ''' Type of Institution '''
        section = last[0]
        section = section.findAll('dd')
        if len(section) == 4:
            # Institution Recognized By
            institution['Recognized By'] = str(section[0].text.strip())
            # Institution Ownership or Control Type
            institution['Ownership or Control Type'] = str(section[1].text.strip())
            # Institution Type of Institution
            institution['Type of Institution'] = str(section[2].text.strip())
        else:
            print('ERROR 07: ' + str(total))

        ''' Affiliated Medical Schools '''
        if len(last) > 1:
            section = last[1]
            section = section.findAll('dd')
            if len(section) == 1:
                # Institution Affiliated Medical Schools
                if not section[0].find('li'):
                    # Normal (no list)
                    institution['Affiliated Medical Schools'] = str(section[0].text.strip())
                else:
                    # List of Affiliated Medical Schools
                    affiliations = section[0].findAll('li')
                    for x, school in enumerate(affiliations):
                        if x+1 == len(affiliations):
                            institution['Affiliated Medical Schools'] += school.text.strip()
                        else:
                            institution['Affiliated Medical Schools'] += school.text.strip() + '; '
            else:
                print('ERROR 08: ' + str(total))
        elif len(last) == 1 and institution['Accredited Programs'] == '0':
            continue
        else:
            print('ERROR 07: ' + str(total))



        '''''''''''''''''''''
        PROGRAMS
        '''''''''''''''''''''
        PROGRAMS = CONTENT.find('div', {'id': 'programsListView'})
        PROGRAMS = PROGRAMS.tbody.findAll('tr', {'class': 'listview-row'})

        for program in PROGRAMS:
            section = program.findAll('td', {'class': 'listview-cell'})
            if len(section) == 6:
                # Sponsored Program - Code
                institution['Sponsored Program - Code'] = str(section[1].text.strip())
                # Sponsored Sponsored Program - Specialty
                institution['Sponsored Program - Specialty'] = str(section[2].text.strip())
                # Sponsored Sponsored Program - PD Last Name
                institution['Sponsored Program - PD Last Name'] = str(section[3].text.strip())

                # Sponsored Sponsored Program - Total Residents Filled
                institution['Sponsored Program - Total Residents Filled'] = str(section[4].text.strip())
                # Sponsored Sponsored Program - Program Link
                institution['Sponsored Program - Program Link'] = str(section[5].a['href'].strip())
            else:
                print('ERROR 08: ' + str(total))


            '''''''''''''''''''''
            ADD TO CSV STRING
            '''''''''''''''''''''
            CSV += "\n"

            # Program Number
            CSV += '"' + str(total) + '",'

            # Institution Number
            CSV += '"' + str(row['total']) + '",'

            '''''''''
            CSV DATA
            '''''''''
            for key, value in institution.items():
                # KEY & VALUE
                if value:
                    CSV += '"' + str(value).replace('"', '') + '",'
                    # print(key, value)
                else:
                    CSV += '"empty",'


            '''''''''''''''''''''
            RESET
            '''''''''''''''''''''
            # print('Total: ' + str(total))

            total += 1
            # Remove final ',' from CSV String
            CSV = CSV[:-1]


            # '''''''''''''''''''''
            # PRINT TO CSV FILE
            # '''''''''''''''''''''
            CSV_out_file = open('ACGME_Programs.csv', 'w')
            CSV_out_file.write(CSV)
            CSV_out_file.close()

    # NO LINK FROM CSV
    else:
        print('ERROR 99: NO "institution_link"')





'''''''''''''''''''''
PRINT CSV
'''''''''''''''''''''
CSV_out_file = open('ACGME_Sponsors.csv', 'w')
CSV_out_file.write(CSV)
CSV_out_file.close()


# class NameThisShit(object):

#     def __init__(self, stuff):
#         self.key_one = stuff['key_one']

# new_thing = NameThisShit(json)
# print(new_thing.key_one)
    

# END - get_ACGME_Sponsors.py