# get_ACGME_Institutions.py


'''
------------------------------
IMPORTS
------------------------------
'''
# Beautiful Soup
# https://www.crummy.com/software/BeautifulSoup/bs4/doc/
import bs4
# URL handling
# https://docs.python.org/3/library/urllib.html
import urllib.request
# JSON 
import json
# CSV Reads
# https://pandas.pydata.org/pandas-docs/stable/generated/pandas.read_csv.html
import pandas
import os.path

import ssl


'''
---------------------------
MAIN
---------------------------
'''

# CSV String
CSV = ''

# Root URL for ACGME website to scrape data from
URL = 'https://apps.acgme.org/ads/Public/Sponsors/Search?stateProvinceId='

root = './'

dirs = [d for d in os.listdir(root) if os.path.isdir(os.path.join(root, d))]


# STATES
states = [
    "Alabama",
    "Alaska",
    "Arizona",
    "Arkansas",
    "California",
    "Colorado",
    "Connecticut",
    "Delaware",
    "District of Columbia",
    "Florida",
    "Georgia",
    "Hawaii",
    "Idaho",
    "Illinois",
    "Indiana",
    "Iowa",
    "Kansas",
    "Kentucky",
    "Louisiana",
    "Maine",
    "Maryland",
    "Massachusetts",
    "Michigan",
    "Minnesota",
    "Mississippi",
    "Missouri",
    "Montana",
    "Nebraska",
    "Nevada",
    "New Hampshire",
    "New Jersey",
    "New Mexico",
    "New York",
    "North Carolina",
    "North Dakota",
    "Ohio",
    "Oklahoma",
    "Oregon",
    "Pennsylvania",
    "Puerto Rico",
    "Rhode Island",
    "South Carolina",
    "South Dakota",
    "Tennessee",
    "Texas",
    "Utah",
    "Vermont",
    "Virginia",
    "Washington",
    "West Virginia",
    "Wisconsin",
    "Wyoming"
]




# User Agent Header request
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)'
}

# SSL Certs
context = ssl._create_unverified_context()


total = 1


for x in range(1, 53):
    print(x)
    

    # Make request to get HTML from server
    request = urllib.request.Request(URL + str(x), None, headers)

    # Request HTML
    source = urllib.request.urlopen(request, context=context).read()

    # Turn raw HTML text to parsable markup
    soup = bs4.BeautifulSoup(source, 'lxml')



    table = soup.find("table", {"id": "institutionListView-listview"})

    institutions = table.find_all("tr")

    # print(institutions)

    institution_code = ''
    institution_name = ''
    institution_dioLastname  = ''
    institution_accreditedPrograms = ''
    institution_status = ''
    institution_link = ''


    for institution in institutions:
        data = institution.find_all("td")
        if len(data) != 0:
            # text = data.find("td")

            # Institution Code
            if data[1].text.isdigit():
                institution_code = str(data[1].text.strip())
            else:
                print("ERROR: (Institution Code):\n")

            # Institution Name
            if not data[2].text.isdigit():
                institution_name = str(data[2].text.strip())
            else:
                print("ERROR: (Institution Name):\n")

            # Institution DIO Lastname
            if not data[3].text.isdigit():
                institution_dioLastname = str(data[3].text.strip())
            else:
                print("ERROR: (Institution DIO Lastname):\n")

            # Institution Number of Accredited Programs
            if data[4].text.isdigit():
                institution_accreditedPrograms = str(data[4].text.strip())
            else:
                print("ERROR: (Number of Accredited Programs):\n")

            # Institution Status
            if not data[5].text.isdigit():
                institution_status = str(data[5].text.strip())
            else:
                print("ERROR: (Status):\n")

            # Institution Link
            if data[6].text == "View Sponsor":
                institution_link = str(data[6].a['href'].strip())
            else:
                print("ERROR: (Link):\n")
            

            


            '''''''''''''''''''''
            BUILD CSV
            '''''''''''''''''''''
            if total > 1:
                CSV += "\n"

            # Number
            CSV += '"' + str(total) + '",'

            # Institution State
            CSV += '"' + str(states[x-1]) + '",'

            # Institution Code
            if institution_code:
                institution_code = institution_code.replace('"', '')
                CSV += '"' + institution_code + '",'
            else:
                print("ERROR: (NO institution_code):\n")
                CSV += '"empty",'


            # Institution Name
            if institution_name:
                institution_name = institution_name.replace('"', '')
                CSV += '"' + institution_name + '",'
            else:
                print("ERROR: (NO institution_name):\n")
                CSV += '"empty",'
            

            # Institution DIO Lastname
            if institution_dioLastname:
                institution_dioLastname = institution_dioLastname.replace('"', '')
                CSV += '"' + institution_dioLastname + '",'
            else:
                print("ERROR: (NO institution_dioLastname):\n")
                CSV += '"empty",'


            # Institution Number of Accredited Programs
            if institution_accreditedPrograms:
                institution_accreditedPrograms = institution_accreditedPrograms.replace('"', '')
                CSV += '"' + institution_accreditedPrograms + '",'
            else:
                print("ERROR: (NO institution_accreditedPrograms):\n")
                CSV += '"empty",'


            # Institution Status
            if institution_status:
                institution_status = institution_status.replace('"', '')
                CSV += '"' + institution_status + '",'
            else:
                print("ERROR: (NO institution_status):\n")
                CSV += '"empty",'


            # Institution Link
            if institution_link:
                CSV += '"https://apps.acgme.org' + institution_link + '"'
            else:
                print("ERROR: (NO institution_link):\n")
                CSV += '"empty"'



            '''''''''''''''''''''
            RESET
            '''''''''''''''''''''
            # Increase Count
            total += 1

            # Reset Values
            institution_code = ''
            institution_name = ''
            institution_dioLastname  = ''
            institution_accreditedPrograms = ''
            institution_status = ''
            institution_link = ''




'''''''''''''''''''''
PRINT CSV
'''''''''''''''''''''
CSV_out_file = open('ACGME_Institutions.csv', 'w')
CSV_out_file.write(CSV)
CSV_out_file.close()




# END - getInstitutions.py