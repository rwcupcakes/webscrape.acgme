# get_ACGME_Programs.py


international = "https://apps.acgme-i.org/ads/public/intl"

'''
------------------------------
IMPORTS
------------------------------
'''
# Beautiful Soup
# https://www.crummy.com/software/BeautifulSoup/bs4/doc/
import bs4
# URL handling
# https://docs.python.org/3/library/urllib.html
import urllib.request
# JSON 
import json
# CSV Reads
# https://pandas.pydata.org/pandas-docs/stable/generated/pandas.read_csv.html
import pandas
import os.path
from io import StringIO

# Making SSL (HTTPS) Requests
import ssl


'''
---------------------------
MAIN
---------------------------
'''
sponsors_csv = './ACGME_Sponsors.csv'
# sponsors_csv = './ACGME_Sponsors.test.csv'

# CSV String
CSV = ''
# Program Counter
total = 1

# User Agent Header request
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)'
}

# SSL Certs
context = ssl._create_unverified_context()



'''
---------------------------
OBJECT
---------------------------
'''
program = {
    # Institution Link
    'Institution - Link': '',
    # Institution ACGME ID Key
    'Institution - ACGME ID Key': '',
    # Institution Sponsor ID
    'Sponsoring - Institution ID': '',
    # Institution State
    'Institution - State': '',
    # Institution Code
    'Institution - Code': '',
    # Institution Name
    'Institution - Name': '',

    # Institution Number of Accredited Programs
    'Institution - Number of Accredited Programs': '',

    # Institution
    'Institution - Accreditation Status': '',
    # Institution Effective Date
    'Institution - Effective Date': '',

    # Institution Last Site Visit Date
    'Institution - Last Site Visit Date': '',
    # Institution Date of Next Site Visit
    'Institution - Date of Next Site Visit': '',
    # Institution Self Study Due Date
    'Institution - Self Study Due Date': '',
    # Institution 10 Year Site Visit
    'Institution - 10 Year Site Visit': '',
    # Institution Last CLER Site Visit Date
    'Institution - Last CLER Site Visit Date': '',
    # Institution Next CLER Site Visit Date
    'Institution - Next CLER Site Visit Date': '',

    # -------------------------------

    # Institution HIPAA Business Associate Agreement On Record
    'Institution - HIPAA Business Associate Agreement On Record': '',
    # Institution Residents Rotate Through This Institution
    'Institution - Residents Rotate Through This Institution': '',

    # Institution CEO
    'Institution - CEO - Name': '',
    'Institution - CEO - Title': '',
    'Institution - CEO - Phone': '',
    'Institution - CEO - Fax': '',
    'Institution - CEO - Email': '',

    # Institution DIO
    'Institution - DIO - Full Name': '',
    'Institution - DIO - Last Name': '',
    'Institution - DIO - Title': '',
    'Institution - DIO - Phone': '',
    'Institution - DIO - Fax': '',
    'Institution - DIO - Email': '',

    # Institution Address
    'Institution - Address': '',
    # Institution Web Site
    'Institution - Web Site': '',
    # Institution Recognized By
    'Institution - Recognized By': '',
    # Institution Ownership or Control Type
    'Institution - Ownership or Control Type': '',
    # Institution Type of Institution
    'Institution - Type of Institution': '',
    # Institution Affiliated Medical Schools
    'Institution - Affiliated Medical Schools': '',
    
    
    # Sponsored Program
    'Sponsored Program - Code': '',
    'Sponsored Program - Specialty': '',
    'Sponsored Program - PD Last Name': '',
    'Sponsored Program - Total Residents Filled': '',
    'Sponsored Program - Program Link': '',


    # PROGRAM
    'Program - Address': '',
    'Program - Web Site': '',
    'Program - Specialty': '',
    'Program - Sponsoring Institution': '',
    'Program - Related Programs': '',
    'Program - Phone': '',
    'Program - Fax': '',
    'Program - Email': '',

    # Program - Director
    'Program - Director - Name': '',
    'Program - Director - Title': '',
    'Program - Director - First Appointed': '',

    # Program - Coordinator
    'Program - Coordinator - Name': '',
    'Program - Coordinator - Title': '',
    'Program - Coordinator - Phone': '',
    'Program - Coordinator - Fax': '',
    'Program - Coordinator - Email': '',

    # Program - Accreditation
    'Program - Accreditation - Original Accreditation Date': '',
    'Program - Accreditation - Accreditation Status': '',
    'Program - Accreditation - Effective Date': '',
    'Program - Accreditation - Accredited Length of Training': '',

    # Program - Osteopathic
    'Program - Osteopathic - Recognition': '',
    'Program - Osteopathic - Effective Date': '',

    'Program - Last Site Visit Date': '',
    'Program - Date of Next Site Visit': '',
    'Program - Self Study Due Date': '',
    'Program - 10 Year Site Visit': ''
}

'''''''''
CSV HEADERS
'''''''''
CSV += '"Program #","Sponsor #","Institution #",'
for key, value in program.items():
    # KEY & VALUE
    CSV += '"' + key+ '",'

# Remove final ',' from CSV String
CSV = CSV[:-1]


# READ CSV (ACGME_Sponsors)
sponsors = pandas.read_csv(
    sponsors_csv, 
	quotechar = '"', 
	skipinitialspace = False, 
	delimiter = ',',
	encoding = 'utf-8',
	index_col = False,
	low_memory = False,
    header=None,
	names=["Total",
            "iTotal",
            "Link",
            "sponsorId_key",
            "Sponsoring Institution ID",
            "State",
            "Code",
            "Name",
            "Accredited Programs",
            "Accreditation Status",
            "Effective Date",
            "Last Site Visit Date",
            "Date of Next Site Visit",
            "Self Study Due Date",
            "10 Year Site Visit",
            "Last CLER Site Visit Date",
            "Next CLER Site Visit Date",
            "HIPAA Business Associate Agreement On Record",
            "Residents Rotate Through This Institution",
            "CEO - Name",
            "CEO - Title",
            "CEO - Phone",
            "CEO - Fax",
            "CEO - Email",
            "DIO - Full Name",
            "DIO - Last Name",
            "DIO - Title",
            "DIO - Phone",
            "DIO - Fax",
            "DIO - Email",
            "Address",
            "Web Site",
            "Recognized By",
            "Ownership or Control Type",
            "Type of Institution",
            "Affiliated Medical Schools",
            "Sponsored Program - Code",
            "Sponsored Program - Specialty",
            "Sponsored Program - PD Last Name",
            "Sponsored Program - Total Residents Filled",
            "Sponsored Program - Program Link"
        ]
    )

#  For each Program
for index, row in sponsors.iterrows():
    print('N: ' + str(row['Total']))
    
    if row['Sponsored Program - Program Link']:
        # Link URL
        URL = 'https://apps.acgme.org' + row['Sponsored Program - Program Link']


        # Program
        program = {
            # Institution Link
            'Institution - Link': row['Link'],
            # Institution ACGME ID Key
            'Institution - ACGME ID Key': row['sponsorId_key'],
            # Institution Sponsor ID
            'Sponsoring - Institution ID': row['Sponsoring Institution ID'],
            # Institution State
            'Institution - State': row['State'],
            # Institution Code
            'Institution - Code': row['Code'],
            # Institution Name
            'Institution - Name': row['Name'],

            # Institution Number of Accredited Programs
            'Institution - Number of Accredited Programs': row['Accredited Programs'],

            # Institution
            'Institution - Accreditation Status': row['Accreditation Status'],
            # Institution Effective Date
            'Institution - Effective Date': row['Effective Date'],

            # Institution Last Site Visit Date
            'Institution - Last Site Visit Date': row['Last Site Visit Date'],
            # Institution Date of Next Site Visit
            'Institution - Date of Next Site Visit': row['Date of Next Site Visit'],
            # Institution Self Study Due Date
            'Institution - Self Study Due Date': row['Self Study Due Date'],
            # Institution 10 Year Site Visit
            'Institution - 10 Year Site Visit': row['10 Year Site Visit'],
            # Institution Last CLER Site Visit Date
            'Institution - Last CLER Site Visit Date': row['Last CLER Site Visit Date'],
            # Institution Next CLER Site Visit Date
            'Institution - Next CLER Site Visit Date': row['Next CLER Site Visit Date'],

            # -------------------------------

            # Institution HIPAA Business Associate Agreement On Record
            'Institution - HIPAA Business Associate Agreement On Record': row['HIPAA Business Associate Agreement On Record'],
            # Institution Residents Rotate Through This Institution
            'Institution - Residents Rotate Through This Institution': row['Residents Rotate Through This Institution'],

            # Institution CEO
            'Institution - CEO - Name': row['CEO - Name'],
            'Institution - CEO - Title': row['CEO - Title'],
            'Institution - CEO - Phone': row['CEO - Phone'],
            'Institution - CEO - Fax': row['CEO - Fax'],
            'Institution - CEO - Email': row['CEO - Email'],

            # Institution DIO
            'Institution - DIO - Full Name': row['DIO - Full Name'],
            'Institution - DIO - Last Name': row['DIO - Last Name'],
            'Institution - DIO - Title': row['DIO - Title'],
            'Institution - DIO - Phone': row['DIO - Phone'],
            'Institution - DIO - Fax': row['DIO - Fax'],
            'Institution - DIO - Email': row['DIO - Email'],

            # Institution Address
            'Institution - Address': row['Address'],
            # Institution Web Site
            'Institution - Web Site': row['Web Site'],
            # Institution Recognized By
            'Institution - Recognized By': row['Recognized By'],
            # Institution Ownership or Control Type
            'Institution - Ownership or Control Type': row['Ownership or Control Type'],
            # Institution Type of Institution
            'Institution - Type of Institution': row['Type of Institution'],
            # Institution Affiliated Medical Schools
            'Institution - Affiliated Medical Schools': row['Affiliated Medical Schools'],
            
            
            # Sponsored Program
            'Sponsored Program - Code': row['Sponsored Program - Code'],
            'Sponsored Program - Specialty': row['Sponsored Program - Specialty'],
            'Sponsored Program - PD Last Name': row['Sponsored Program - PD Last Name'],
            'Sponsored Program - Total Residents Filled': row['Sponsored Program - Total Residents Filled'],
            'Sponsored Program - Program Link': row['Sponsored Program - Program Link'],


            # PROGRAM
            'Program - Address': '',
            'Program - Web Site': '',
            'Program - Specialty': '',
            'Program - Sponsoring Institution': '',
            'Program - Related Programs': '',
            'Program - Phone': '',
            'Program - Fax': '',
            'Program - Email': '',

            # Program - Director
            'Program - Director - Name': '',
            'Program - Director - Title': '',
            'Program - Director - First Appointed': '',

            # Program - Coordinator
            'Program - Coordinator - Name': '',
            'Program - Coordinator - Title': '',
            'Program - Coordinator - Phone': '',
            'Program - Coordinator - Fax': '',
            'Program - Coordinator - Email': '',

            # Program - Accreditation
            'Program - Accreditation - Original Accreditation Date': '',
            'Program - Accreditation - Accreditation Status': '',
            'Program - Accreditation - Effective Date': '',
            'Program - Accreditation - Accredited Length of Training': '',

            # Program - Osteopathic
            'Program - Osteopathic - Recognition': '',
            'Program - Osteopathic - Effective Date': '',

            # Program - Site Visit
            'Program - Last Site Visit Date': '',
            'Program - Date of Next Site Visit': '',
            'Program - Self Study Due Date': '',
            'Program - 10 Year Site Visit': ''
        }


        # REQUEST
        '''''''''''''''''''''
        REQUEST
        '''''''''''''''''''''

        # Make request to get HTML from server
        request = urllib.request.Request(URL, None, headers)

        # Request HTML
        source = urllib.request.urlopen(request, context=context).read()

        # Turn raw HTML text to parsable markup
        soup = bs4.BeautifulSoup(source, 'lxml')

        CONTENT = soup.find('div', {'id': 'content-panel'})
        '''''''''''''''''''''
        WEB DATA
        '''''''''''''''''''''
        # DIVS
        DIVS = CONTENT.findAll('div', {'class': None})



        ''' Program - Info '''
        section = DIVS[0]
        # Program - (Address, Web Site, Specialty, Related Programs, Phone, Fax, Email)
        if section.find('address'):
            address = section.address.text.strip()
            address = address.replace('  ', '')
            address = address.replace('\r', '')
            address = address.split('\n')
            for i, line in enumerate(address):
                if i+1 == len(address):
                    # Program - Address
                    program['Program - Address'] += str(line.strip())
                else:
                    # Program - Address
                    program['Program - Address'] += str(line.strip()) + ' '
        else:
            print('MISSING (Program - Address): ' + str(total))

        # Program - Web Site
        program['Program - Web Site'] = str(section.a['href'].strip())

        # SECTIONS
        section = section.findAll('dl')
        sectionThis = section[0].findAll('dd')
        if len(sectionThis) == 2:
            # Program - Specialty
            program['Program - Specialty'] = str(sectionThis[0].text.strip())
            # Program - Sponsoring Institution
            program['Program - Sponsoring Institution'] = str(sectionThis[1].text.strip())
        else:
            print('MISSING (Program - (Specialty, Sponsoring Institution)): ' + str(total))

        # sectionThis = section[1].findAll('dd')
        # if len(sectionThis) == 1:
        #     # Program - Related Programs
        #     program['Program - Related Programs'] = str(sectionThis[0].text.strip())
        # else:
        #     print('MISSING (Program - Related Programs): ' + str(total))
        
        
        sectionThis = section[2].findAll('dd')
        if len(sectionThis) == 3:
            # Program - Phone
            program['Program - Phone'] = str(sectionThis[0].text.strip())
            # Program - Fax
            program['Program - Fax'] = str(sectionThis[1].text.strip())
            # Program - Email
            program['Program - Email'] = str(sectionThis[2].text.strip())
        else:
            print('MISSING (Program - (Phone, Fax, Email)): ' + str(total))



        ''' Program - Director '''
        section = DIVS[1]

        name_title = section.ul.findAll('li')
        if len(name_title) == 2:
            # Program - Director - Name
            program['Program - Director - Name'] = str(name_title[0].text.strip())
            # Program - Director - Title
            program['Program - Director - Title'] = str(name_title[1].text.strip())
        else:
            print('MISSING (Program - Director - (Name, Title)): ' + str(total))

        # print(name_title)
        
        if section.find('dl'):
            # Program - Director - First Appointed
            program['Program - Director - First Appointed'] = str(section.dl.dd.text.strip())
        else:
            print('MISSING (Program - Director - First Appointed): ' + str(total))




        ''' Program - Coordinator '''
        section = DIVS[2]

        # Program - Coordinator (Name, Title)
        if section.find('ul'):
            name_title = section.ul.findAll('li')
            if len(name_title) == 2:
                # Program - Coordinator - Name
                program['Program - Coordinator - Name'] = str(name_title[0].text.strip())
                # Program - Coordinator - Title
                program['Program - Coordinator - Title'] = str(name_title[1].text.strip())
            else:
                print('MISSING (Program - Coordinator (Name, Title)): ' + str(total))
        else:
            print('MISSING (Program - Coordinator): ' + str(total))

        # Program - Coordinator (Phone, Fax, Email)
        section = section.findAll('dd')
        if len(section) == 2:
            # Program - Coordinator - Phone
            program['Program - Coordinator - Phone'] = str(section[0].text.strip())
            # Program - Coordinator - Fax
            # program['Program - Coordinator - Fax'] = ''
            # Program - Coordinator - Email
            program['Program - Coordinator - Email'] = str(section[1].text.strip())
        else:
            print('MISSING (Program - Coordinator (Phone, Fax, Email)): ' + str(total))


        ''' Program - Accreditation '''
        section = DIVS[3]
        # Program - Accreditation - (Original Accreditation Date, Accreditation Status, Effective Date, Accredited Length of Training)
        section = section.findAll('dd')
        if len(section) == 6:
            # Program - Accreditation - Original Accreditation Date
            program['Program - Accreditation - Original Accreditation Date'] = str(section[0].text.strip())
            # Program - Accreditation - Accreditation Status
            program['Program - Accreditation - Accreditation Status'] = str(section[1].text.strip())
            # Program - Accreditation - Effective Date
            program['Program - Accreditation - Effective Date'] = str(section[2].text.strip())
            # Program - Accreditation - Accredited Length of Training
            program['Program - Accreditation - Accredited Length of Training'] = str(section[3].text.strip())
        else:
            print('MISSING (Program - Accreditation): ' + str(total))


        ''' Program - Osteopathic '''
        section = DIVS[3]
        section = section.findAll('i')
        if len(section) == 2:
            # Program - Osteopathic - Recognition
            program['Program - Osteopathic - Recognition'] = str(section[0].text.strip())
            # Program - Osteopathic - Effective Date
            program['Program - Osteopathic - Effective Date'] = str(section[1].text.strip())
        else:
            print('MISSING (Program - Osteopathic): ' + str(total))


        ''' Program - Site Visit '''
        section = DIVS[4]
        # Program - (Last Site Visit Date, Date of Next Site Visit, Self Study Due Date, 10 Year Site Visit)
        section = section.findAll('dd')
        if len(section) == 4:
            # Program - Last Site Visit Date
            program['Program - Last Site Visit Date'] = str(section[0].text.strip())
            # Program - Date of Next Site Visit
            program['Program - Date of Next Site Visit'] = str(section[1].text.strip())
            # Program - Self Study Due Date
            program['Program - Self Study Due Date'] = str(section[2].text.strip())
            # Program - 10 Year Site Visit
            program['Program - 10 Year Site Visit'] = str(section[3].text.strip())
        else:
            print('MISSING (Program - Site Visit): ' + str(total))

        

        '''''''''''''''''''''
        ADD TO CSV STRING
        '''''''''''''''''''''
        CSV += "\n"

        # Program #
        CSV += '"' + str(total) + '",'

        # Sponsor #
        CSV += '"' + str(row['Total']) + '",'

        # Institution #
        CSV += '"' + str(row['iTotal']) + '",'

        '''''''''
        CSV DATA
        '''''''''
        for key, value in program.items():
            # KEY & VALUE
            if value:
                CSV += '"' + str(value).replace('"', '') + '",'
                # print(key, value)
            else:
                CSV += '"empty",'


        '''''''''''''''''''''
        RESET
        '''''''''''''''''''''
        # print('Total: ' + str(total))

        total += 1
        # Remove final ',' from CSV String
        CSV = CSV[:-1]

        '''''''''''''''''''''
        PRINT CSV
        '''''''''''''''''''''
        CSV_out_file = open('ACGME_Programs.csv', 'w')
        CSV_out_file.write(CSV)
        CSV_out_file.close()
        

    # NO LINK FROM CSV
    else:
        print('ERROR 99: NO "institution_link"')


# '''''''''''''''''''''
# PRINT CSV
# '''''''''''''''''''''
# CSV_out_file = open('ACGME_Programs.csv', 'w')
# CSV_out_file.write(CSV)
# CSV_out_file.close()

# END - get_ACGME_Programs.py