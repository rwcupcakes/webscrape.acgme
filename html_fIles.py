# INSTALL
# sudo pip3 install beautifulsoup4
# sudo pip3 install lxml

import os.path
import bs4

root = './AAMC Programs'

dirs = [d for d in os.listdir(root) if os.path.isdir(os.path.join(root, d))]

CSV = ''

csv_program_type = ''
csv_program_cycle = ''
csv_program_specialty = ''
csv_program_sub_specialty = ''
csv_program_name = ''
csv_program_status = ''
csv_city = ''
csv_state = ''
csv_program_acgme_id = ''
csv_website = ''
csv_new_program = ''



for folder in dirs:

	# print('\n' + folder)
	folderName = folder;
	folderName = folderName.split(' - ')

	# Program Type
	csv_program_type = folderName[0]
	# Program Cycle
	csv_program_cycle = folderName[1]


	for filename in os.listdir(root + '/' + folder):

		if '.html' in filename:
			fullname = filename.replace('.html', '')
			# print(filename)
			if '(' in fullname:
				fullname = fullname.split(' (')

				# Program Sub Specialty
				csv_program_sub_specialty = fullname[0].strip()
				# Program Specialty
				csv_program_specialty = fullname[1].strip()
				if ')' in csv_program_specialty:
					csv_program_specialty = csv_program_specialty.replace(')', '').strip()
			
			else:
				# Program Specialty
				csv_program_specialty = fullname.strip()
				# Program Sub Specialty - No subspecialty
				csv_program_sub_specialty = ''

			nextFile = root + '/' + folder + '/' + filename
			print(nextFile)
			# IGNORE ALL NONASCII (encoding='utf-8', errors='ignore') 
			with open(nextFile, 'r', encoding='utf-8', errors='ignore') as htmlfile:
				html = htmlfile.read()

				# SOUP
				soup = bs4.BeautifulSoup(html, 'lxml')

				table = soup.find('table', {'class': 'pplcell'})
				tablePrograms = table.find('table')
				# Program Information
				tableRows = tablePrograms.find_all('tr', {'class': 'bordersm'})

				for tr in tableRows:
					tableDatas = tr.find_all('td')
					programDataArray = []

					alink = 'None'
					if tr.find('a'):
						alink = tr.find('a')['href']
					programDataArray.append(alink)

					for td in tableDatas:
						table_data = td.text
						# print(td)
						
						if table_data != '':
							table_data = ' '.join(table_data.split())
							programDataArray.append(table_data)

					# ERROR: Missing Data or Misaligned
					# if len(programDataArray) != 5:

					# 	raise Exception('Missing data in ' + nextFile + ' with this data: ' + programDataArray)

					# Program State
					csv_state = programDataArray[1]
					# Program City
					csv_city = programDataArray[2]
					# Program Name
					csv_program_name = programDataArray[3]
					# Program ACGME Id
					csv_program_acgme_id = programDataArray[4]
					# Program Status - participating
					csv_program_status = programDataArray[5]
					# Website
					csv_website = programDataArray[0]


					if '!' in csv_program_name:
						csv_program_name = csv_program_name.replace(' New Program!', '')
						csv_new_program = 'New Program'



					# ADD TO CSV
					CSV += '"' + csv_program_type + '",'
					CSV += '"' + csv_program_cycle + '",'
					CSV += '"' + csv_program_specialty + '",'
					CSV += '"' + csv_program_sub_specialty + '",'
					CSV += '"' + csv_program_name + '",'
					CSV += '"' + csv_program_status + '",'
					CSV += '"' + csv_city + '",'
					CSV += '"' + csv_state + '",'
					CSV += '"' + csv_program_acgme_id + '",'
					CSV += '"' + csv_new_program + '",'
					CSV += '"' + csv_website + '"'
					CSV += '\n'

					
					csv_program_name = ''
					csv_program_status = ''
					csv_city = ''
					csv_state = ''
					csv_program_acgme_id = ''
					csv_new_program = ''
					csv_website = ''

			
		csv_program_specialty = ''
		csv_program_sub_specialty = ''

	csv_program_type = ''
	csv_program_cycle = ''


# print(CSV)

CSV_out_file = open('ERAS Programs.csv', 'w')
CSV_out_file.write(CSV)
CSV_out_file.close()


