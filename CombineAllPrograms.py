# CombineAllPrograms.py

import pandas
from io import StringIO
programs_programs_csv = './programs.csv'
eras_programs_csv = './ERAS Programs.csv'


programs_programs = pandas.read_csv(programs_programs_csv, 
	quotechar = '"', 
	skipinitialspace = False, 
	delimiter = ',', 
	encoding = 'utf-8',
	index_col = False,
	header=0,
	low_memory = False,
	names=['file_name',
		'institute_number', 
		'institute_name', 
		'page', 
		'num', 
		'program_number', 
		'program_name', 
		'specialty', 
		'director_first_name', 
		'director_middle_name', 
		'director_last_name', 
		'director_suffix', 
		'program_email', 
		'program_phone', 
		'program_fax', 
		'address_street', 
		'address_city', 
		'address_state', 
		'address_zip', 
		'address_other', 
		'address_secondary', 
		'address_title'])

# programs_programs2 = programs_programs.set_index('program_number')
# print(programs_programs['program_number']['1402631204'])
# print(programs_programs2.loc['program_number', : ])
# print(programs_programs2.loc["1402631204":"1404112358","institute_number":"page"])


eras_programs = pandas.read_csv(eras_programs_csv, 
	quotechar = '"', 
	skipinitialspace = False,
	delimiter = ',', 
	encoding = 'utf-8',
	index_col = 8,
	low_memory = False,
	header=None,
	names=['program_type', 
		'program_cycle', 
		'program_specialty', 
		'program_sub_specialty', 
		'program_name', 
		'program_status', 
		'address_city', 
		'address_state', 
		'program_number', 
		'new_program',
		'website'])


# print(programs_programs.loc[:,"program_number"].mode())

CSV = ''

# CSV Columns
program_number = ''
program_order = ''
program_type = ''
program_cycle = ''
program_specialty = ''
program_sub_specialty = ''
program_name = ''
program_status = ''
institute_number = ''
institute_name = ''
program_email = ''
program_phone = ''
program_fax = ''
address_street = ''
address_city = ''
address_state = ''
address_zip = ''
director_first_name = ''
director_middle_name = ''
director_last_name = ''
director_suffix = ''
new_program = ''
website = ''

# SET HEADER
CSV += '"program_number",'
CSV += '"program_order",'
CSV += '"program_type",'
CSV += '"program_cycle",'
CSV += '"program_specialty",'
CSV += '"program_sub_specialty",'
CSV += '"program_name",'
CSV += '"program_status",'
CSV += '"institute_number",'
CSV += '"institute_name",'
CSV += '"program_email",'
CSV += '"program_phone",'
CSV += '"program_fax",'
CSV += '"address_street",'
CSV += '"address_city",'
CSV += '"address_state",'
CSV += '"address_zip",'
CSV += '"director_first_name",'
CSV += '"director_middle_name",'
CSV += '"director_last_name",'
CSV += '"director_suffix",'
CSV += '"new_program",'
CSV += '"website"'
CSV += '\n'

overlapingProgramIDs = []

# print(programs_programs)

for index, row in programs_programs.iterrows():

	# RESET DATA
	program_number = ''
	program_order = ''
	program_type = ''
	program_cycle = ''
	program_specialty = ''
	program_sub_specialty = ''
	program_name = ''
	program_status = ''
	institute_number = ''
	institute_name = ''
	program_email = ''
	program_phone = ''
	program_fax = ''
	address_street = ''
	address_city = ''
	address_state = ''
	address_zip = ''
	director_first_name = ''
	director_middle_name = ''
	director_last_name = ''
	director_suffix = ''
	new_program = ''
	website = ''



	# Program Info
	program_number = str(row['program_number'])
	# program_order = ''
	# program_type = ''
	# program_cycle = ''
	if row['specialty']:
		program_specialty = row['specialty']

	# program_sub_specialty = ''
	if row['program_name']:
		program_name = row['program_name']

	# program_status = ''
	# Institution Info
	if row['institute_number']:
		institute_number = row['institute_number']

	if row['institute_name']:
		institute_name = row['institute_name']

	# Email, Phone, Fax
	if row['program_email']:
		program_email = row['program_email']

	if row['program_phone']:
		program_phone = row['program_phone']

	if row['program_fax']:
		program_fax = row['program_fax']

	# Address
	if row['address_street']:
		address_street = row['address_street']

	if row['address_city']:
		address_city = row['address_city']

	if row['address_state']:
		address_state = row['address_state']

	if row['address_zip']:
		address_zip = row['address_zip']

	# Director
	if row['director_first_name']:
		director_first_name = row['director_first_name']

	if row['director_middle_name']:
		director_middle_name = row['director_middle_name']

	if row['director_last_name']:
		director_last_name = row['director_last_name']

	if row['director_suffix']:
		director_suffix = row['director_suffix']
	# New Program
	# new_program = ''
	# Website
	# website = ''

	try:
		eras_programs['program_type'][program_number]

		overlapingProgramIDs.append(program_number)

		# Program Type
		if  eras_programs['program_type'][program_number]:
			program_type = eras_programs['program_type'][program_number]
			program_type = program_type.split(' ')
			program_order = program_type[0]
			program_type = program_type[1]

		# Program Cycle
		if eras_programs['program_cycle'][program_number]:
			program_cycle = eras_programs['program_cycle'][program_number]

		# Program Specialty
		if eras_programs['program_specialty'][program_number]:
			program_specialty = eras_programs['program_specialty'][program_number]

		# Program Sub Specialty
		if eras_programs['program_sub_specialty'][program_number]:
			program_sub_specialty = eras_programs['program_sub_specialty'][program_number]

		# Program Name Replace
		if eras_programs['program_name'][program_number]:
			program_name = eras_programs['program_name'][program_number]

		# Program Status
		if eras_programs['program_status'][program_number]:
			program_status = eras_programs['program_status'][program_number]

		# Address City
		if address_city == '':
			if eras_programs['address_city'][program_number]:
				address_city = eras_programs['address_city'][program_number]

		# Address State
		if address_state == '':
			if eras_programs['address_state'][program_number]:
				address_state = eras_programs['address_state'][program_number]

		# Is New Program
		if eras_programs['new_program'][program_number]:
			new_program = eras_programs['new_program'][program_number]

		# Website
		if eras_programs['website'][program_number]:
			website = eras_programs['website'][program_number]

	except:
		# print(program_number)
		pass

	# ADD TO CSV
	if str(program_number) != 'nan':
		CSV += '"' + str(program_number) + '",'
	else:
		CSV += '"",'

	if str(program_order) != 'nan':
		CSV += '"' + str(program_order) + '",'
	else:
		CSV += '"",'

	if str(program_type) != 'nan':
		CSV += '"' + str(program_type) + '",'
	else:
		CSV += '"",'

	if str(program_cycle) != 'nan':
		CSV += '"' + str(program_cycle) + '",'
	else:
		CSV += '"",'

	if str(program_specialty) != 'nan':
		CSV += '"' + str(program_specialty) + '",'
	else:
		CSV += '"",'

	if str(program_sub_specialty) != 'nan':
		CSV += '"' + str(program_sub_specialty) + '",'
	else:
		CSV += '"",'

	if str(program_name) != 'nan':
		CSV += '"' + str(program_name) + '",'
	else:
		CSV += '"",'

	if str(program_status) != 'nan':
		CSV += '"' + str(program_status) + '",'
	else:
		CSV += '"",'

	if str(institute_number) != 'nan':
		CSV += '"' + str(institute_number) + '",'
	else:
		CSV += '"",'

	if str(institute_name) != 'nan':
		CSV += '"' + str(institute_name) + '",'
	else:
		CSV += '"",'

	if str(program_email) != 'nan':
		CSV += '"' + str(program_email) + '",'
	else:
		CSV += '"",'

	if str(program_phone) != 'nan':
		CSV += '"' + str(program_phone) + '",'
	else:
		CSV += '"",'

	if str(program_fax) != 'nan':
		CSV += '"' + str(program_fax) + '",'
	else:
		CSV += '"",'

	if str(address_street) != 'nan':
		CSV += '"' + str(address_street) + '",'
	else:
		CSV += '"",'

	if str(address_city) != 'nan':
		CSV += '"' + str(address_city) + '",'
	else:
		CSV += '"",'

	if str(address_state) != 'nan':
		CSV += '"' + str(address_state) + '",'
	else:
		CSV += '"",'

	if str(address_zip) != 'nan':
		CSV += '"' + str(address_zip) + '",'
	else:
		CSV += '"",'

	if str(director_first_name) != 'nan':
		CSV += '"' + str(director_first_name) + '",'
	else:
		CSV += '"",'

	if str(director_middle_name) != 'nan':
		CSV += '"' + str(director_middle_name) + '",'
	else:
		CSV += '"",'

	if str(director_last_name) != 'nan':
		CSV += '"' + str(director_last_name) + '",'
	else:
		CSV += '"",'

	if str(director_suffix) != 'nan':
		CSV += '"' + str(director_suffix) + '",'
	else:
		CSV += '"",'

	if str(new_program) != 'nan':
		CSV += '"' + str(new_program) + '",'
	else:
		CSV += '"",'

	if str(website) != 'nan':
		CSV += '"' + str(website) + '"'
	else:
		CSV += '"",'

	CSV += '\n'



# THE REST OF THE ERAS PROGRAMS
for index, row in eras_programs.iterrows():
	if index not in overlapingProgramIDs:
		# RESET DATA
		program_number = ''
		program_order = ''
		program_type = ''
		program_cycle = ''
		program_specialty = ''
		program_sub_specialty = ''
		program_name = ''
		program_status = ''
		institute_number = ''
		institute_name = ''
		program_email = ''
		program_phone = ''
		program_fax = ''
		address_street = ''
		address_city = ''
		address_state = ''
		address_zip = ''
		director_first_name = ''
		director_middle_name = ''
		director_last_name = ''
		director_suffix = ''
		new_program = ''
		website = ''

		program_number = str(index)

		if  eras_programs['program_type'][program_number]:
			program_type = eras_programs['program_type'][program_number]
			program_type = program_type.split(' ')
			program_order = program_type[0]
			program_type = program_type[1]

		# Program Cycle
		if eras_programs['program_cycle'][program_number]:
			program_cycle = eras_programs['program_cycle'][program_number]

		# Program Specialty
		if eras_programs['program_specialty'][program_number]:
			program_specialty = eras_programs['program_specialty'][program_number]

		# Program Sub Specialty
		if eras_programs['program_sub_specialty'][program_number]:
			program_sub_specialty = eras_programs['program_sub_specialty'][program_number]

		# Program Name Replace
		if eras_programs['program_name'][program_number]:
			program_name = eras_programs['program_name'][program_number]

		# Program Status
		if eras_programs['program_status'][program_number]:
			program_status = eras_programs['program_status'][program_number]

		# Address City
		if address_city == '':
			if eras_programs['address_city'][program_number]:
				address_city = eras_programs['address_city'][program_number]

		# Address State
		if address_state == '':
			if eras_programs['address_state'][program_number]:
				address_state = eras_programs['address_state'][program_number]

		# Is New Program
		if eras_programs['new_program'][program_number]:
			new_program = eras_programs['new_program'][program_number]

		# Website
		if eras_programs['website'][program_number]:
			website = eras_programs['website'][program_number]

		# ADD TO CSV
		if str(program_number) != 'nan':
			CSV += '"' + str(program_number) + '",'
		else:
			CSV += '"",'

		if str(program_order) != 'nan':
			CSV += '"' + str(program_order) + '",'
		else:
			CSV += '"",'

		if str(program_type) != 'nan':
			CSV += '"' + str(program_type) + '",'
		else:
			CSV += '"",'

		if str(program_cycle) != 'nan':
			CSV += '"' + str(program_cycle) + '",'
		else:
			CSV += '"",'

		if str(program_specialty) != 'nan':
			CSV += '"' + str(program_specialty) + '",'
		else:
			CSV += '"",'

		if str(program_sub_specialty) != 'nan':
			CSV += '"' + str(program_sub_specialty) + '",'
		else:
			CSV += '"",'

		if str(program_name) != 'nan':
			CSV += '"' + str(program_name) + '",'
		else:
			CSV += '"",'

		if str(program_status) != 'nan':
			CSV += '"' + str(program_status) + '",'
		else:
			CSV += '"",'

		if str(institute_number) != 'nan':
			CSV += '"' + str(institute_number) + '",'
		else:
			CSV += '"",'

		if str(institute_name) != 'nan':
			CSV += '"' + str(institute_name) + '",'
		else:
			CSV += '"",'

		if str(program_email) != 'nan':
			CSV += '"' + str(program_email) + '",'
		else:
			CSV += '"",'

		if str(program_phone) != 'nan':
			CSV += '"' + str(program_phone) + '",'
		else:
			CSV += '"",'

		if str(program_fax) != 'nan':
			CSV += '"' + str(program_fax) + '",'
		else:
			CSV += '"",'

		if str(address_street) != 'nan':
			CSV += '"' + str(address_street) + '",'
		else:
			CSV += '"",'

		if str(address_city) != 'nan':
			CSV += '"' + str(address_city) + '",'
		else:
			CSV += '"",'

		if str(address_state) != 'nan':
			CSV += '"' + str(address_state) + '",'
		else:
			CSV += '"",'

		if str(address_zip) != 'nan':
			CSV += '"' + str(address_zip) + '",'
		else:
			CSV += '"",'

		if str(director_first_name) != 'nan':
			CSV += '"' + str(director_first_name) + '",'
		else:
			CSV += '"",'

		if str(director_middle_name) != 'nan':
			CSV += '"' + str(director_middle_name) + '",'
		else:
			CSV += '"",'

		if str(director_last_name) != 'nan':
			CSV += '"' + str(director_last_name) + '",'
		else:
			CSV += '"",'

		if str(director_suffix) != 'nan':
			CSV += '"' + str(director_suffix) + '",'
		else:
			CSV += '"",'

		if str(new_program) != 'nan':
			CSV += '"' + str(new_program) + '",'
		else:
			CSV += '"",'

		if str(website) != 'nan':
			CSV += '"' + str(website) + '"'
		else:
			CSV += '"",'

		CSV += '\n'




# ALL FILES Processed

# Write out CSV String
CSV_out_file = open('ALLFUCKINGPROGRAMS.csv', 'w')
CSV_out_file.write(CSV)
CSV_out_file.close()










#  FROM ERAS CSV
# csv_program_order = ''
# csv_program_type = ''
# csv_program_cycle = ''
# csv_program_specialty = ''
# csv_program_sub_specialty = ''
# csv_program_name = ''
# csv_program_status = ''
# csv_address_city = ''
# csv_address_state = ''
# csv_program_number = ''
# csv_new_program = ''
# csv_website = ''

#  FROM PROGRAMS CSV
# csv_institute_number = ''
# csv_institute_name = ''
# csv_director_first_name = ''
# csv_director_middle_name = ''
# csv_director_last_name = ''
# csv_director_suffix = ''
# csv_program_email = ''
# csv_program_phone = ''
# csv_program_fax = ''
# csv_address_street = ''
# csv_address_zip = ''
















